// Audio files
let cap_click = document.getElementById("click_sound")
let tick_tack = document.getElementById("ticking_sound")
tick_tack.loop = true

// clock-hands
const minuteHand = document.querySelector('[data-minute-hand]')
const secondHand = document.querySelector('[data-second-hand]')

const start_btn = document.querySelector('.toggle-cap')
const cap = document.querySelector('.cap')
let serial = document.querySelector('.serial')
serial.innerHTML = 0 + 's'

let started = false
let restart = false
let start_time = 0

start_btn.addEventListener('click', () => {
  if(restart) {
    start_time = 0
    restart = false
    restart_clock()
    cap_click.play()
    return
  }
  if(started) {
    started = false
    restart = true
    cap_click.play()
    tick_tack.pause()
    clearInterval(interval)
    tick_tack.currentTime = 0
  } else {
    started = true
    interval = setInterval(startClock, 250)
    cap_click.play()
    tick_tack.play()
  }
})

function startClock() {
  // If reached 600 seconds
  if(start_time/4 > 600) {
    started = false
    restart = true
    cap_click.play()
    tick_tack.pause()
    clearInterval(interval)
  } else {
    const minutesRatio = (start_time / 600) /4
    const secondsRatio = (start_time / 100) /4
    setMinuteRotation(minuteHand, minutesRatio)
    setSecondsRotation(secondHand, secondsRatio)
    serial.innerHTML = Math.floor(start_time/4) + 's'
    start_time++
  }
}

function restart_clock() {
  start_time = 0
  const minutesRatio = (start_time / 650) /4
  const secondsRatio = (start_time / 100) /4
  setMinuteRotation(minuteHand, minutesRatio)
  setSecondsRotation(secondHand, secondsRatio)
}

function setMinuteRotation(element, rotationRatio) {
  element.style.setProperty('--rotation', rotationRatio * 150)
}
function setSecondsRotation(element, rotationRatio) {
  element.style.setProperty('--rotation', rotationRatio * 360)
}


