// Audio files
const cap_click = document.getElementById("click_sound")
const ticking = document.getElementById("ticking_sound")
ticking.loop = true 

// Toggle cap
const cap = document.querySelector('.toggle-cap')

// When clicked on the cap
cap.addEventListener('click', () => {
  cap_click.play()
})