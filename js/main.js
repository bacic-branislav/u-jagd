// Audio files
let cap_click = document.getElementById("click_sound")
let tick_tack = document.getElementById("ticking_sound")
tick_tack.loop = true

// clock-hands
const minuteHand = document.querySelector('[data-minute-hand]')
const secondHand = document.querySelector('[data-second-hand]')

const start_btn = document.querySelector('.toggle-cap')
const time_passed = document.getElementById('time_passed')
const cap = document.querySelector('.cap')

started = false
restart = false
start_time = 0

start_btn.addEventListener('click', () => {
  if(restart) {
    start_time = 0
    time_passed.innerHTML = start_time
    restart = false
    restart_clock()
    cap_click.play()
    return
  }
  if(started) {
    started = false
    restart = true
    cap_click.play()
    tick_tack.pause()
    clearInterval(interval)
    tick_tack.currentTime = 0
  } else {
    started = true
    interval = setInterval(startClock, 1000)
    cap_click.play()
    tick_tack.play()
  }
})

function startClock() {
  // If reached 600 seconds
  if(start_time > 600) {
    started = false
    restart = true
    cap_click.play()
    tick_tack.pause()
    clearInterval(interval)
  } else {
    start_time++
    time_passed.innerHTML = start_time
    const minutesRatio = (start_time / 600)
    const secondsRatio = (start_time / 100)
    setMinuteRotation(minuteHand, minutesRatio)
    setSecondsRotation(secondHand, secondsRatio)
  }
}

function restart_clock() {
  start_time = 0
  const minutesRatio = (start_time / 650)
  const secondsRatio = (start_time / 100)
  setMinuteRotation(minuteHand, minutesRatio)
  setSecondsRotation(secondHand, secondsRatio)
}

function setMinuteRotation(element, rotationRatio) {
  element.style.setProperty('--rotation', rotationRatio * 150)
}
function setSecondsRotation(element, rotationRatio) {
  element.style.setProperty('--rotation', rotationRatio * 360)
}




function calculate_speed() {
  let ship_length = document.getElementById('lenght_slider').value
  let seconds = document.getElementById('time_slider').value
  let result = document.querySelector('.result')

  if(seconds == 0) {
    result.innerHTML = ''
    return
  }
  // speed = (2 * ship length) / seconds
  let speed = (1.96 * ship_length) / seconds 
  result.innerHTML = Math.floor(speed) + ' knots'
}


function calculate_aob() {
  let behind_target = document.getElementById('behind_target').checked
  let zoom_level = document.getElementById('zoom').checked
  let zoom = zoom_level ? 1.5 : 6 
  let vertical_marks = document.getElementById('marks_height').value
  let horizontal_marks = document.getElementById('marks_width').value
  let mast_height = document.getElementById('mast_height').value
  let ship_length = document.getElementById('ship_length').value
  const result_distance = document.querySelector('.result_distance')
  const result_aob = document.querySelector('.result_aob')
  const pointer = document.querySelector('.pointer')

  if(marks_height == 0 || mast_height ==0 || ship_length == 0 || marks_width == 0) {
    result_distance.innerHTML =  '-'
    result_aob.innerHTML =  '-'
    return
  } 

  // Distance = ((mast height * 100) / height in scope) * 4 if zoomed
  if(zoom == 6) 
    var distance = ((mast_height * 100) / vertical_marks) * 4
  else 
    var distance = (mast_height * 100) / vertical_marks
  
  // AOB calculations
  let aspect_ratio_reference = ship_length / mast_height
  let aspect_radio_opservant = horizontal_marks / vertical_marks
  let aspect_ratio = (aspect_radio_opservant / aspect_ratio_reference) * 100
  aspect_ratio = Math.floor(aspect_ratio)
  let aob

  
  console.log(aspect_ratio)
  pointer.style.left = aspect_ratio + "%"


  // if(aspect_ratio < 25) {
  //   aob = aspect_ratio / 1.75
  // } else if(aspect_ratio >= 25 && aspect_ratio < 50) {
  //   aob = aspect_ratio / 1.67
  // } else if(aspect_ratio >= 50 && aspect_ratio < 75) {
  //   aob = aspect_ratio / 1.53
  // } else if(aspect_ratio >= 75 && aspect_ratio < 85) {
  //   aob = aspect_ratio / 1.44
  // } else if(aspect_ratio >= 85 && aspect_ratio < 90) {
  //   aob = aspect_ratio / 1.40
  // } else if(aspect_ratio >= 90 && aspect_ratio < 93) {
  //   aob = aspect_ratio / 1.38
  // } else if(aspect_ratio >= 93 && aspect_ratio < 95) {
  //   aob = aspect_ratio / 1.3
  // }else if(aspect_ratio >= 95 && aspect_ratio < 97) {
  //   aob = aspect_ratio / 1.25
  // } else if(aspect_ratio >= 97 && aspect_ratio < 98) {
  //   aob = aspect_ratio / 1.24
  // } else if(aspect_ratio >= 98 && aspect_ratio < 99) {
  //   aob = aspect_ratio / 1.22
  // } else if(aspect_ratio >= 99 && aspect_ratio < 100) {
  //   aob = aspect_ratio / 1.15
  // } else if(aspect_ratio >= 100) {
  //   aob = aspect_ratio / 1.11
  // }
  

  // AoB
  if(behind_target)
    result_aob.innerHTML = Math.round(aspect_ratio + 90) + '%'
  else
    result_aob.innerHTML = Math.round(aspect_ratio) + '%'
  // Range
  result_distance.innerHTML = Math.round(distance) + 'm'
}